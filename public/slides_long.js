FOOTER = "Un approccio DevOps per la produzione dell'architettura<br>" +
	"di Marco Ferrara"

LOCAL = location.protocol == 'file:' ? true : false
//LOCAL = false;

document.getElementById("footer").innerHTML = FOOTER;
var choose = function(a,b){return LOCAL?a:b}

var slides = [
	[

		[
			'Oltre il BIM', '', '',
			`
			<p class="fragment subtitle" style="font-size: .5em;
				font-style: italic;">
				<span class="" style="display: inline-block;" 
					data-id="processo_operativo"> Per un processo operativo </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_tracciato"> tracciato </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_non_distruttivo"> non distruttivo </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_discretizzato"> discretizzato </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_distribuito"> distribuito </span>
				<br>
			<span class="fragment subtitle" style="font-size:.66em">
				Superare l'approccio pre-digitale nel processo di produzione 
				dell'architettura</span>
				</p>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Process_features', 'style="display: none"', '',
			`
			<p class="subtitle" style="font-size: .5em; font-style: italic; 
				text-align: left; font-size: 1em">
				<span class="" style="display: inline-block;" 
					data-id="processo_operativo"> Un processo operativo </span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_tracciato">tracciato
					<span class="fragment note bottom_auto">documentato in
						ogni passaggio operativo e decisionale</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_non_distruttivo">non distruttivo
					<span class="fragment note bottom_auto">
						capace di permettere il ripristino di ogni stato
						operativo precedente</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_discretizzato">discretizzato
					<span class="fragment note bottom_auto">
						costituito unicamente da operazioni piccole,
						semplici e coerenti</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_distribuito">distribuito
					<span class="fragment note bottom_auto">
						in modo da consentire il lavoro in parallelo di 
						tutti gli operatori</span>
						</span>
				</p>
			`,
			'', {'data-auto-animate': null}
		],
		/*
		[
			'Indice', '',
			`
			...
			`
		],
		*/
		[
			'Project_data', 'style="display: none"', '',
			`
			<p class="">
				Sviluppare un progetto architettonico 
				significa gestirne i dati qualificanti:
			</p>
			<div class="fragment">
				<ul class="">
					<li>dati <strong>formali</strong></li>
					<li>dati <strong>spaziali</strong></li>
					<li>dati <strong>estetici</strong></li>
					<li>dati <strong>temporali</strong></li>
				</ul>
				<ul class="" style="vertical-align: top">
					<li>dati <strong>qualitativi</strong></li>
					<li>dati <strong>culturali</strong></li>
					<li>dati <strong>quantitativi</strong></li>
				</ul>
			</div>
			`
		],
		[
			'Relations_data', 'style="display: none"', '',
			`
			<p class="">
				Ma anche gestire i dati relativi a...
			</p>
			<ul class="fragment">
				<li>al <strong>processo decisionale</strong></li> 
				<li>al <strong>metodo operativo</strong></li> 
				<li>all'<strong>evoluzione del progetto</strong> nel tempo</li> 
				<li>ai <strong>ruoli e alle responsabilità</strong> degli attori 
					coinvolti</li> 
				<li>ai <strong>rapporti e alle comunicazioni</strong> 
					con committenti, artigiani, imprese, PA, fornitori</li>
			</ul>
			`
		],
		[
			'Data_management', 'style="display: none"', '',
			`
			<span class="">
				In quest'ottica è possibile<br>
				<em>interpretare il processo progettuale come</em>
			</span>
			<div class="fragment">
				<span class="" style="display: block">
					<b>la gestione di una articolata quantità di</b>
				<div class="">
				<span class="" style="display: block" data-id="info">
					<strong>informazioni</strong>
				</span>
				<span class="" style="display: block">
					il cui approdo è l'oggetto costruito   
				</span>
			</div>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Data_based_output', 'style="display: none"', '',
			`
			<span class="">
				Dalla capacità di
			</span>
			<span class="" style="display: block">
				<b>processare</b>, <b>controllare</b>, <b>governare</b> tali
			</span>
			<div class="" style="display: block">
				<span class="" style="display: block" data-id="info">
					<strong>informazioni</strong>
				</span>
				<span class="" style="display: block">
					dipende l'esito del processo produttivo
				</span>
			</div>
			`,
			'', {'data-auto-animate': null} //, 'data-transition': '"slide"'}
		],
		[
			'Show_method', 'style="display: none"', '',
			`
			<p>
				Il presente lavoro illustra 
				<span style="display:inline-block" data-id="method">un 
					<strong>approccio operativo</strong></span> 
				praticabile 
				<span style="display:inline-block" data-id="based">
					basato su</span> 
				<b>una gestione intelligente e proficua dei</b>
				<span style="display:inline-block" data-id="data">
					<strong>dati</strong></span> 
				che danno forma al progetto...
			</p>
			<p class="fragment">
				...e teso a <b>rendere il processo produttivo<br> 
				<span style="display:inline-block" data-id="effective">
					più</b> <strong>efficiente</strong></span>
				<span style= "font-size:.66em"><sup><i>(e quindi più 
					<b>rapido</b> ed <b>economico</b>)</i></sup></span><br>
				<span style="display:inline-block" data-id="robust">
					e <b>più</b> <strong>robusto</strong></span> 
				<span style="font-size:.66em"><sup><i>(e quindi più 
					<b>fluido</b> e <b>sicuro</b>)</i></sup></span>
			</p>
			`,
			'', {'data-auto-animate': null, 'data-auto-animate-id': 'method'} 
		],
		[
			'Method_short', 'style="display: none"', '',
			`
			<span style="display:inline-block" data-id="method">un 
				<strong>approccio operativo</strong></span> 
			<span style="display:inline-block" data-id="based">basato su</span>
			<span style="display:inline-block" data-id="data"> 
				<strong>dati</strong></span> 
			<span style="display:inline-block" data-id="effective">più 
				<strong>efficiente</strong></span> 
			<span style="display:inline-block" data-id="robust">e più 
				<strong>robusto</strong></span>
			`,
			'', {'data-auto-animate': null, 'data-auto-animate-id': 'method'} 
		],
	],
	[
		[
			'Sul processo di sviluppo',
		],
		[
			'Metadata', 'style="display: none"', '',
			`
			<p>
				Intendere il progetto ed il processo che lo ha generato 
				in termini di <b>dati</b> significa associare i relativi 
				<strong>metadati</strong> ad ogni:
			</p>
			<ul>
				<li class="fragment">file,</li>
				<li class="fragment">evento,</li>
				<li class="fragment">collaboratore,</li>
			</ul>
			<ul>
				<li class="fragment">ipotesi di lavoro,</li>
				<li class="fragment">comunicazione,</li>
				<li class="fragment">discussione, ect...</li>
			</ul>
			`
		],
		[
			'VCS', 'style="display: none"', '',
			`
		    <p>
				Per farlo è possibile adottare 
				<b>strumenti di controllo versione</b> 
				(<em>Version Control System</em> o <em>VCS</em>) capaci di<br>
				<strong>tracciare l'evolversi del processo di 
				elaborazione</strong>
			</p>
			`,
		],
		[
			'VCS_img', 'style="display: none"', '',
			``,
			'', {"data-background": "img/git_log.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom"}
		],
		[
			'VCS_features', 'style="display: none"', '',
			`
		    <p>
				L'utilizzo di un VCS permette di <b>governare</b> e 
				<b>dirigere</b> il processo di sviluppo consentendo, 
				tra l'altro, di:
			</p>
			`,
		],
		[
			'VCS_avoid_duplicate', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>evitare duplicazioni di files</strong> per prove o 
				copie di salvataggio: <i>tutte le versioni di ogni file, 
				superate o alternative, sono infatti <b>sempre facilmente 
				recuperabili</b>.</i>
		    </li></ul>
			`,
		],
		[
			'VCS_avoid_duplicate_img', 'style="display: none"', '',
			`
			<div class="fragment" style="width: 80%; margin: 0 auto 0 auto; 
				background: #ffffffd4;">
				<span>In tal modo la cartella di lavoro si 
				mantiene <b>pulita ed ordinata</b> ed i contenuti del progetto 
				restano <b>facilmente accessibili</b>.</span><br>
				<span class="fragment">Analogamente è possibile <b>ripristinare 
				qualsiasi stato precedente</b> dell'intero progetto.</span>
			</div></li>
			
			`,
			'', {"data-background": "img/recover_previous.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
		[
			'VCS_branches', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>sviluppare linee di sviluppo</strong> <i>(branches)</i> 
				<strong>alternative</strong> e coesistenti.   
		    </li></ul>
			`,
		],
		[
			'VCS_branches_img', 'style="display: none"', '',
			`
			<div class="fragment" style="width: 80%; margin: 0 auto 0 auto; 
				background: #ffffffd4;">
				Le differenti versioni del progetto sono 
				<b>facilmente confrontabili...</b>
			</div>
			`,
			'', {"data-background": "img/compare.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
		[
			'VCS_merging_0', 'style="display: none"', '',
			`
			...ed è possibile <b>far confluire più linee di sviluppo in 
			un'unica versione</b><br>
			<span class="fragment">Ad esempio...</span>
			`,
		],
		[
			'VCS_merging_1', 'style="display: none"', '',
			`<div>...un <i>branch</i> può gestire i materiali...</div>`,
			'text-align:left; color:white; text-shadow: 0px 0px 5px black;', 
			{"style": "height:100%; top:0;",
				"data-background-video": "img/model_merge_1_mat.webm", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",},
			'style="height:100%; width:70%; margin-left:15px;' + 
					'display:flex; justify-content: flex-start;"'
		],
		[
			'VCS_merging_2', 'style="display: none"', '',
			`<div>...in un altro <i>branch</i> si può sviluppare una 
				disposizione alternativa...</div>`,
			'text-align:left; color:white; text-shadow: 0px 0px 5px black;', 
			{"style": "height:100%; top:0",
				"data-background-video": "img/model_merge_2_disp.webm", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",},
			'style="height:100%; width:70%; margin-left:15px;' + 
					'display:flex; justify-content: flex-start;"'
		],
		[
			'VCS_merging_3', 'style="display: none"', '',
			`<div>...e <i>fondere</i> i due <i>branches</i> in una
				versione che recepisce entrambe le variazioni</div>
			`,
			'text-align:left; color:white; text-shadow: 0px 0px 5px black;', 
			{"style": "height:100%; top:0",
				"data-background-video": "img/model_merge_3_all.webm", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",},
			'style="height:100%; width:70%; margin-left:15px;' + 
					'display:flex; justify-content: flex-start;"'
		],
		[
			'VCS_merging_note', 'style="display: none"', '',
			`
			L'utilizzo di <i>branches</i> differenti ed indipendenti è, 
			inoltre, la base per<br> <b>favorire l'elaborazione 
			<strong>parallela</strong>, <strong>simultanea</strong> e 
			<strong>distribuita </strong></b> tra più collaboratori.
		    </li>
			</ul>
			`,
		],
		[
			'VCS_doc', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>documentare l'intero processo di sviluppo</strong> 
				rendendo tutto il materiale prodotto facilmente 
				identificabile ed accessibile<br>
				<span class="fragment">Questo consente di <b>effettuare 
					ricerche</b> nella <i>storia</i> del progetto... 
				</span>
		    </li></ul>
			`,
		],
		[
			'VCS_search', 'style="display: none"', '',
			``,
			'', {"data-background": "img/git_search.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
		[
			'VCS_stats', 'style="display: none"', '',
			`
			...o generare <b><i>analytics</i></b> capaci di misurare e 
			rappresentare le attività svolte
			`,
		],
		[
			'VCS_stats', 'style="display: none"', '',
			``,
			'', {"data-background": "img/gource.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
	],
	[
		[
			'Sui contenuti del progetto',
		],
		[
			'Comp_think', 'style="display: none"', '',
			`
			<span class="fragment fade-out">Pensare il progetto 
			<strong>in forma di dati</strong> significa</span><br>
			predisporre l'oggetto architettonico ad essere gestito<br>
			<strong>in termini computazionali</strong>
			`,
		],
		[
			'3d_as_database', 'style="display: none"', '',
			`
			<span style="display: inline-block" data-id="3d_model">
				Il <strong>modello 3d</strong></span> 
			diviene così<br>
			<span style="display: inline-block" data-id="database">
				<strong>una base dati</strong></span> strutturata 
				<b>semanticamente connotata</b><br>
			<div class="fragment">
				<span style="display: inline-block" class="" data-id="to_store">
					per <strong>conservare</strong> e d</span>a cui 
				<span style="display: inline-block" data-id="to_get">
					<strong>estrarre</strong></span><br> 
				<span style="display: inline-block" data-id="project_info"> 
					<b>tutte le informazioni relative al progetto</b></span>
			</div>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'3d_to_get_info', 'style="display: none"', '',
			`
			<div class="fragment fade-out">
				<span style="display: inline-block" data-id="3d_model">
					Il <strong>modello 3d</strong></span>,
				<span style="display: inline-block" data-id="database">
					<strong>una base dati</strong></span>
				<span style="display: inline-block" class="" data-id="to_store">
					per <strong>conservare</strong> ed</span>
			</div>
			<span style="display: inline-block" data-id="to_get">
				<strong>estrarre</strong></span><br> 
			<span style="display: inline-block" data-id="project_info"> 
				<b>tutte le informazioni relative al progetto</b></span><br>
			<span class="fragment" style= "font-size:.66em"><sup>
				<i>(come computi e documentazione grafica)</i></sup></span>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Auto_drawing', 'style="display: none"', '',
			``,
			'', {"data-background": "img/auto_drawing.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
		[
			'digital_consistency', 'style="display: none"', '',
			`
			<span>
				Discretizzato in
				<strong>piccole unità logiche indicizzate</strong>
				raccolte in <b>librerie</b> organizzate,<br>
				l'oggetto progettato diviene<br>
				<strong>coerente al medium digitale</strong></span><br>
			<span class="fragment">
				...e di conseguenza...</span>
			`,
		],
		[
			'Digital_consistency_advantages', 'style="display: none"', '',
			`
			<ul>
				<li class="">
					il progetto risulta <b>scalabile e modulare</b>
				</li>
				<li class="fragment">
					<b>si riduce il rischio di corruzione dei files</b> 
					o perdita di dati
				</li>
				<li class="fragment">
					<b>si facilita il lavoro collaborativo in parallelo</b>
				</li>
				<li class="fragment">
					<b>si agevola l'aggiornamento</b> parziale e mirato 
					<b>dei contenuti</b>
				</li>
				<li class="fragment">
					le modifiche vengono <b>automaticamente propagate</b> su 
					tutti i documenti collegati
				</li>
			</ul>
			`,
		],
		[
			'Two_principles', 'style="display: none"', '',
			`
			<span>
				Ridotto ad un <b>sistema di dati</b> logicamente organizzati e 
				interconnessi, il progetto si presta ad essere sviluppato 
				in coerenza con due principi chiave:
				</span><br>
			<ul class="">
				<li class="fragment"><strong>SSoT (Single source of Truth)</strong></li>
				<li class="fragment"><strong>Release early, release often</strong></li>
				</ul>
			`,
		],
		[
			'SSoT', 'style="display: none"', '',
			`
			<strong>SSoT (Single source of Truth)</strong><br>
			<span>
				Ogni informazione deve risiedere unicamente in <b>un'unica 
				sorgente</b>...<br> </span>
			<span class="fragment">
				...i file sono specifici e <b>monofunzionali</b> mentre la 
				duplicazione delle informazioni è ridotta al minimo.
			</span>
			`,
		],
		[
			'Release_early_release_often', 'style="display: none"', '',
			`
			<strong>Release early, release often</strong><br>
			<span>
				Ridurre il lavoro all'esecuzione di <b>tante, piccole 
				operazioni circoscritte</b> permette di distribuire i dati 
				prodotti in maniera rapida e frequente...<span><br>
				<span class="fragment">
				...in tal modo tutto il gruppo di lavoro può disporre 
				del<b>le informazioni più aggiornate e corrette</b> ed 
				evitare, così, errori e spreco di tempo
			</span>
			`,
		],
		[
			'Edits_propagation', 'style="display: none"', '',
			``,
			'', {"data-background": "img/css_changes.gif", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",}
		],
		[
			'Comp_think_2', 'style="display: none"', '',
			`
			<span class="fragment fade-out">Pensare il progetto 
			<strong>in forma di dati</strong> significa</span><br>
			predisporre l'oggetto architettonico ad essere gestito<br>
			<strong>in termini computazionali</strong>
			`,
		],
		[
			'Parametric_modeling', 'style="display: none"', '',
			`
			La <b>forma</b> è <strong>ricodificata</strong>,<br>
			lo <b>spazio</b> si <strong>parametrizza</strong>,<br>
			la <b>genesi geometrica</b> è un <strong>algoritmo</strong>
			`,
		],
		[
			'Parametric_modeling_img', 'style="display: none"', '',
			``,
			'', {"style": "height:100%; top:0;",
				"data-background-video": "img/parametric_modeling.webm", 
				"data-background-size": "contain",
				"data-background-transition": "zoom",},
		],
		[
			'Computational_method', 'style="display: none"', '',
			`
			L'approccio computazionale <b><i>spinge l'elaborazione verso 
			le logiche della programmazione</i></b>...<br>
			<span style="display: inline-block" class="fragment" 
				data-id="makes_process">...e rende il processo progettuale:
				</span>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Computation_advantages', 'style="display: none"', '',
			`
			<span style="display: inline-block" data-id="makes_process">
				...e rende il processo progettuale:</span>
			<ul>
				<li class="fragment">
					<b>più produttivo</b> <sup style="font-size:.66em"><i>
						riducendo le operazioni meccaniche e ripetitive</i></sup>
				</li>
				<li class="fragment">
					<b>più sicuro</b> <sup style="font-size:.66em">
						<i>riducendo gli errori</i></sup>
				</li>
				<li class="fragment">
					<b>più fluido</b> <sup style="font-size:.66em">
						<i>riducendo gli intoppi</i></sup>
				</li>
				<li class="fragment">
					<b>meno stressante</b> <sup style="font-size:.66em">
						<i>riducendo la fatica</i></sup>
				</li>
				<li class="fragment">
					<b>di maggiore qualità</b> <sup style="font-size:.66em">
						<i>liberando risorse per lo sviluppo creativo</i></sup>
				</li>
			</ul>
			`,
			'', {'data-auto-animate': null}
		],
	],
	[
		[
			'Sull\'organizzazione del lavoro',
		],
		[
			'lavoro_collaborativo_asincrono', 'style="display: none"', '',
			`
			<p>

				Il <b>tracciamento</b> del processo produttivo e l'<b>indicizzazione</b> 
				(semi-automatica) di ogni evento, file, persona, commento, discussione, 
				argomento, etc. generato durante lo sviluppo spingono l'assetto operativo 
				verso logiche improntate al lavoro:
				<ul>
					<li class="fragment"><strong>collaborativo</strong></li>
					<li class="fragment"><strong>asincrono</strong></li>
				</ul>
				</p>
			`,
			'', {'data-auto-animate': null, }
		],
		[
			'Workflow_optimization', 'style="display: none"', '',
			`
			<p>
				Mutuando strumenti provenienti dall'ambito dello sviluppo software 
				open source e facendo proprie alcune pratiche della metodologia 
				<i>Agile</i> è possibile implementare il flusso di lavoro di pratiche 
				di ottimizzazione e controllo come:
				<ul>
					<li class="fragment"><strong>commit commenting</strong></li>
					<li class="fragment"><strong>kanban</strong> e <strong>issues</strong></li>
					<li class="fragment"><strong>merge/pull request</strong></li>
					<li class="fragment"><strong>assignee, reviewers, participants</strong></li>
					<li class="fragment"><strong>pair programming</strong></li>
				</ul>
				</p>
			`,
			'', {'data-auto-animate': null, }
		],
		[
			'Workflow_optimization', 'style="display: none"', '',
			`
			<p>
				L'adozione di tali modalità operative porta ad<br>
				<b>una piena <strong>trasparenza</strong> nel processo decisionale</b><br>
				<span class="fragment"> da cui deriva<br>
				<b>un'<strong>autoresponsabilizzazione</strong> dell'intero gruppo di lavoro</b></span><br>
				<span class="fragment"> che pone le premesse per<br>
				<b>un ambiente di lavoro <strong>sano e collaborativo</strong>
					basato sulla fiducia reciproca</b></span>
				</p>
			`,
			'', {'data-auto-animate': null, }
		],

		//[
		//	// TODO -> format this
		//	'Release_early_and_often', 'style="display: none"', '',
		//	`
		//	La condivisione <b>rapida</b> e <b>frequente</b>* degli stati di 
		//	avanzamento del lavoro consente di:
		//	<ul>
		//		<li>sviluppare l'elaborazione in parallelo senza colli di bottiglia</li>
		//		<li>effettuare un migliore e tempestivo controllo qualità</li>
		//		<li>circostanziare chiaramente le fasi di sviluppo su cui intervenire</li>
		//		<li>raffinare l'indicizzazione degli eventi del processo</li>
		//	</ul>
		//	*Release early, release often
		//	`,
		//],
		// TODO
		// single source of truth
		// img -> issue board

	],

	[
		['Fine']
	]
]
