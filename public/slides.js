FOOTER = "Un approccio DevOps per la produzione dell'architettura"

LOCAL = location.protocol == 'file:' ? true : false
//LOCAL = false;

document.getElementById("footer").innerHTML = FOOTER;
var choose = function(a,b){return LOCAL?a:b}

var slides = [
	//[
		[
			'Oltre il BIM', '', '',
			`
			<p class="fragment subtitle" style="font-size: .5em;
				font-style: italic;">
				<span class="" style="display: inline-block;" 
					data-id="processo_operativo"> Per un processo operativo </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_tracciato"> tracciato </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_non_distruttivo"> non distruttivo </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_discretizzato"> discretizzato </span>,
				<span class="" style="display: inline-block;" 
					data-id="processo_distribuito"> distribuito </span>
				<br>
			<span class="fragment subtitle" style="font-size:.66em">
				Superare l'approccio pre-digitale nel processo di produzione 
				dell'architettura</span>
				</p>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Process_features', 'style="display: none"', '',
			`
			<p class="subtitle" style="font-size: .5em; font-style: italic; 
				text-align: left; font-size: 1em">
				<span class="" style="display: inline-block;" 
					data-id="processo_operativo"> Un processo operativo </span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_tracciato">tracciato
					<span class="fragment note bottom_auto">documentato in
						ogni passaggio operativo e decisionale</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_non_distruttivo">non distruttivo
					<span class="fragment note bottom_auto">
						capace di permettere il ripristino di ogni stato
						operativo precedente</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_discretizzato">discretizzato
					<span class="fragment note bottom_auto">
						costituito unicamente da operazioni piccole,
						semplici e coerenti</span>
				</span><br>
				<span class="" style="display: inline-block; width: 100%" data-id="processo_distribuito">distribuito
					<span class="fragment note bottom_auto">
						tale da consentire il lavoro in parallelo di 
						tutti gli operatori</span>
						</span>
				</p>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Project_data', 'style="display: none"', '',
			`
			<p class="">
				Sviluppare un progetto architettonico 
				significa gestirne i dati qualificanti:
			</p>
			<div class="fragment">
				<ul class="">
					<li>dati <strong>formali</strong></li>
					<li>dati <strong>spaziali</strong></li>
					<li>dati <strong>estetici</strong></li>
					<li>dati <strong>temporali</strong></li>
				</ul>
				<ul class="" style="vertical-align: top">
					<li>dati <strong>qualitativi</strong></li>
					<li>dati <strong>culturali</strong></li>
					<li>dati <strong>quantitativi</strong></li>
				</ul>
			</div>
			`
		],
		[
			'Relations_data', 'style="display: none"', '',
			`
			<p class="">
				Ma anche gestire i dati relativi a...
			</p>
			<ul class="fragment">
				<li>al <strong>processo decisionale</strong></li> 
				<li>al <strong>metodo operativo</strong></li> 
				<li>all'<strong>evoluzione del progetto</strong> nel tempo</li> 
				<li>ai <strong>ruoli e alle responsabilità</strong> degli attori 
					coinvolti</li> 
				<li>ai <strong>rapporti e alle comunicazioni</strong> 
					con committenti, artigiani, imprese, PA, fornitori</li>
			</ul>
			`
		],
		[
			'Data_management', 'style="display: none"', '',
			`
			<span class="">
				In quest'ottica è possibile<br>
				<em>interpretare il processo progettuale come</em>
			</span>
			<div class="fragment">
				<span class="" style="display: block">
					<b>la gestione di una articolata quantità di</b>
				<div class="">
				<span class="" style="display: block" data-id="info">
					<strong>informazioni</strong>
				</span>
				<span class="" style="display: block">
					il cui approdo è l'oggetto costruito   
				</span>
			</div>
			`,
			'', {'data-auto-animate': null}
		],
		[
			'Data_based_output', 'style="display: none"', '',
			`
			<span class="">
				Dalla capacità di
			</span>
			<span class="" style="display: block">
				<b>processare</b>, <b>controllare</b>, <b>governare</b> tali
			</span>
			<div class="" style="display: block">
				<span class="" style="display: block" data-id="info">
					<strong>informazioni</strong>
				</span>
				<span class="" style="display: block">
					dipende l'esito del processo produttivo
				</span>
			</div>
			`,
			'', {'data-auto-animate': null} //, 'data-transition': '"slide"'}
		],
		[
			'Show_method', 'style="display: none"', '',
			`
			<p>
				Il presente lavoro traccia, sinteticamente,
				<span style="display:inline-block" data-id="method">un 
					<strong>approccio operativo</strong></span> 
				praticabile 
				<span style="display:inline-block" data-id="based">
					basato su</span> 
				<b>una gestione intelligente e proficua dei</b>
				<span style="display:inline-block" data-id="data">
					<strong>dati</strong></span> 
				che danno forma al progetto...
			</p>
			<p class="fragment">
				...e teso a <b>rendere il processo produttivo<br> 
				<span style="display:inline-block" data-id="effective">
					più</b> <strong>efficiente</strong></span>
				<span style= "font-size:.66em"><sup><i>(e quindi più 
					<b>rapido</b> ed <b>economico</b>)</i></sup></span><br>
				<span style="display:inline-block" data-id="robust">
					e <b>più</b> <strong>robusto</strong></span> 
				<span style="font-size:.66em"><sup><i>(e quindi più 
					<b>fluido</b> e <b>sicuro</b>)</i></sup></span>
			</p>
			`,
			'', {'data-auto-animate': null, 'data-auto-animate-id': 'method'} 
		],
		[
			'Method_short', 'style="display: none"', '',
			`
			<span style="display:inline-block" data-id="method">un 
				<strong>approccio operativo</strong></span> 
			<span style="display:inline-block" data-id="based">basato su</span>
			<span style="display:inline-block" data-id="data"> 
				<strong>dati</strong></span> 
			<span style="display:inline-block" data-id="effective">più 
				<strong>efficiente</strong></span> 
			<span style="display:inline-block" data-id="robust">e più 
				<strong>robusto</strong></span>
			`,
			'', {'data-auto-animate': null, 'data-auto-animate-id': 'method'} 
		],
	//],
	//[
		[
			'Sul processo di sviluppo',
		],
		[
			'Vcs_adoption', 'style="display: none"', '',
			`
			<h3><b>AZIONE:</b></h3>
			<p class="fragment">
			Adozione di un<br><strong>Version Control System</strong>
			</p>
			`
		],
		[
			'Vcs', 'style="display: none"', '',
			`
			<p>
			Un <b><i>VCS</i></b> è uno strumento capace di<br>
			<strong>tracciare l'evoluzione dell'elaborazione progettuale</strong><br>
			associando <b>metadati</b> (autore, data, contenuti) ad ogni 
			avanzamento del processo di sviluppo
			</p>
			`
		],
		[
			'Vcs_advantages', 'style="display: none"', '',
			`
			<h3><b>BENEFICI:</b></h3>
			<p class="fragment">
				L'utilizzo di un <b><i>VCS</i></b> permette di<br>
				<strong>governare e dirigere il processo di sviluppo</strong>
				consentendo, nel contempo, di:
			</p>
			`,
		],
		[
			'VCS_avoid_duplicate', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>evitare duplicazioni di files e contenuti</strong>
				<div class="fragment" style="text-align: center">
				permettendo di:<br>
				<ul>
					<li><b>ridurre i rischi di errore</b> relativi alla proliferazione
					di versioni temporanee o superate dei file</li>
					<li><b>mantenere la cartella di lavoro ordinata</b> e, di conseguenza,
					rendere il materiale prodotto più facilmente accessibile</li>
				</ul>
		    </li></ul>
			`,
		],
		[
			'VCS_backups', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>salvaguardare il lavoro prodotto</strong><br>
				creando numerosi e documentati <b>backup</b>
		    </li></ul>
			`,
		],
		[
			'VCS_branches', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>sviluppare linee di sviluppo parallele</strong>
				o alternative, capaci di interagire tra di loro...
				<div class="fragment" style="text-align: center">
				...e di conseguenza...<br>
				<span>favorire <b>l'elaborazione collaborativa parallela, 
					simultanea e distribuita</b></span>
				</div>
		    </li></ul>
			`,
		],
		[
			'VCS_documenting', 'style="display: none"', '',
			`
		    <ul><li>
				<strong>documentare l'intero processo di sviluppo</strong>
				rendendo il materiale prodotto facilmente 
				identificabile ed accessibile...<br>
				<div class="fragment" style="">
				...e consentendo di:<br>
				<ul>
					<li>effettuare ricerche nella "storia" del progetto e 
					facilitare il <b>recupero delle informazioni</b></li>
					<li>permettere di generare <i>analytics</i> capaci 
					<b>di misurare e rappresentare le attività svolte</b></li>
				</ul>
		    </li></ul>
			`,
		],
	//],
	//[
		[
			'Sui contenuti del progetto',
		],
		[
			'Linkings_and_standards', 'style="display: none"', '',
			`
			<h3><b>AZIONE:</b></h3>
			<p class="fragment">
				Implementazione strutturata di <br>
				<strong>sistemi linkati</strong> e <strong>standards</strong>
			</p>
			`,
		],
		[
			'Computational_thinking', 'style="display: none"', '',
			`
			<p>
			In ambito cad, un utilizzo sistematico e pervasivo di <b>blocchi</b> e <b>xref</b>
			e l'adozione di un sistema di standards per <b>layers, pennini, 
			denominazioni, etc...</b> solido, versatile, autoesplicativo ed 
			incrementabile<br>
			<i>costituisce la base per<br>
			<strong>pensare il progetto in forma di dati</strong><br>
			e gestirlo in <b>termini computazionali</b></i>
			</p>
			`
		],
		[
			'Little_logic_units', 'style="display: none"', '',
			`
			<p>
			Implementare tale approccio si traduce in una <b>discretizzazione</b>
			in <strong>piccole unità logiche</strong> <b>interconnesse e raccolte in 
			librerie organizzate</b> relative...
			<ul>
				<li>al progetto (ad es.: <i>serramenti, arredi, impianti, etc...</i>)</li>
				<li>alle sue formalizzazioni (ad es.: <i>piante, alzati, stampa, etc...</i>)</li>
			</ul>
			</p>
			`
		],


		[
			'Main_principles', 'style="display: none"', '',
			`
			Coerentemente con tale impianto:
			<ul>
				<li>
				Ogni informazione deve risiedere in <strong>un'unica sorgente</strong>,
				in files specifici e <b>monofunzionali</b> <b>evitando la duplicazione 
				delle informazioni</b>
				</li>
				<li>
				Il lavoro deve essere inteso come esecuzione di tante, <strong>piccole 
				operazioni circoscritte</strong> i cui esiti siano rapidamente e frequentemente 
				ridistribuiti tra i collaboratori
				</li>
			</ul>
			`,
		],
		[
			'Digital_consistency_advantages', 'style="display: none"', '',
			`
			<h3><b>BENEFICI:</b></h3>
			<div>
				<ul class="fragment fade-in-then-out" style="position: absolute; left:0">
					<li class="">
						il progetto risulta <b>scalabile e modulare</b>
					</li>
					<li class="">
						si riduce il rischio di <b>corruzione dei files</b> 
						o perdita di dati
					</li>
					<li class="">
						si evita la <b>proliferazione di versioni superate</b> di files e contenuti
					</li>
				</ul>
				<ul class="fragment fade-in" style="position: absolute; left:0">
					<li class="">
						si facilita il <b>lavoro collaborativo in parallelo</b>
					</li>
					<li class="">
						si agevola l'<b>aggiornamento parziale, immediato e mirato</b>
						dei contenuti
					</li>
					<li class="">
						le modifiche vengono <b>automaticamente propagate</b> su 
						tutti i documenti collegati
					</li>
				</ul>
			</div>
			`,
		],
	//],
	//[
		[
			'Sull\'organizzazione del lavoro',
		],
		[
			'Project_managment_platform', 'style="display: none"', '',
			`
			<h3><b>AZIONE:</b></h3>
			<p class="fragment">
			Utilizzo di una <br>
			<strong>piattaforma di Project Management</strong><br>
			connessa al VCS
			<span class="note">e inspirata alle metodologie Agile e DevOps</span>
			</p>
			`
		],
		[
			'DevOps_platform', 'style="display: none"', '',
			`
			<p>
				Le piattaforme <i>DevOps</i><br><b>interfacciano il tracciamento del processo 
				produttivo con le interazioni tra gli attori coinvolti</b>
				</p>
				<p class="fragment">
				Ogni evento, file, persona, commento, discussione, argomento, etc... prodotto 
				durante lo sviluppo del progetto è, pertanto, <b>indicizzato, linkabile e 
				relazionabile</b> all'interno del processo
				</p>
			`,
			'', {'data-auto-animate': null, }
		],
		[
			'Async_collaborative_work', 'style="display: none"', '',
			`
			<p>
			Il lavoro si presta, così, ad essere sviluppato prevalentemente in forma<br>
			<strong>collaborativa</strong> e <strong>asincrona</strong>
			</p>
			`,
			'', {'data-auto-animate': null, }
		],
		[
			'DevOps_advantages', 'style="display: none"', '',
			`
			<h3><b>BENEFICI:</b></h3>
			<div>
				<ul class="fragment fade-in-then-out" style="position: absolute; left:0">
					<li class="">
						tracciamento in <b>un unico luogo</b> di tutte le informazioni
						relative al progetto e al processo 
					</li>
					<li class="">
					 	<b>organizzazione del lavoro per tasks</b>,
						indicizzati, linkabili e automaticamente aggiornati
					</li>
					<li class="">
						<b>chiara definizione di ruoli e responsabilità</b> per ogni elaborazione
					</li>
				</ul>
				<ul class="fragment fade-in" style="position: absolute; left:0">
					<li class="">
						<b>processo decisionale strutturato, trasparente, efficiente e 
						controllato</b>
					</li>
					<li class="">
						<b>coinvolgimento dell'intero gruppo di lavoro</b> in ogni fase 
						dello sviluppo
					</li>
				</ul>
			</div>
			`,
		],
	//],

	//[
		['Fine']
	//]
]
